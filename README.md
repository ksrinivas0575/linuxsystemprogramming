# OS Internals & Linux System Programming 

## Table of Contents

* OS Architecture, Kernel, System calls [click here](intro)
* Process Management [click here](process)
* Signal Handling [click here](signals)
* Threads [click here](threads)
* File System Concepts [click here](fs)
* Interprocess Communication [click here](ipc)
  * Semaphores
  * Mutex
  * Shared Memory
  * Message Queues
  * Pipes/Fifos
* Memory Management [click here](mm)

# Suggested Reading
* https://linuxjourney.com ==> process, kernel
* https://linuxhint.com/category/system-calls/
* https://beej.us/guide/bgipc/
* https://www.tutorialspoint.com/inter_process_communication/index.htm
